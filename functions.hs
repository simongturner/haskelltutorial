-- basic function - takes a parameter (x), and does something to it
doubleMe x = x + x

--function that uses previous function on it's two parameters
doubleUs x y = doubleMe x + doubleMe y

--function that uses basic conditional statement
doubleSmallNumber x = if x > 100 then x else x * 2

--similar function, and hence defined with the sameName' which is used for an improved or stricter version of a function
doubleSmallNumber' x = (if x > 100 then x else x * 2) +1

--takes parameter xs, substitutes into function as x, applies conditional statement
fizzBuzz xs = [ if x `mod` 3 == 0 then "fizz" else if x `mod` 5 == 0 then "buzz" else "neither" | x <- xs ]

--remade version of the length function. Takes a parameter xs, substitutes parameter string for nothing, makes everything equal to 1, adds all the ones together
length' xs = sum [1 | _ <- xs]


removeNonUpperCase :: String -> String
removeNonUpperCase string = [ c | c <- string, c `elem` ['A'..'Z']]

addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z

--factorial :: Integer -> Integer
--factorial n = product [1..n]

circumference :: Float -> Float
circumference r = 2 * pi * r

circumference' :: Double -> Double
circumference' r = 2 * pi * r

lucky 7 = "lucky number seven"
lucky x = "you're out of luck, pal"

factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1)

numberSwitch :: (Integral a) => a -> String
numberSwitch 5 = "That's 5"
numberSwitch 4 = "That's 4"
numberSwitch 3 = "That's 3"
numberSwitch 2 = "That's 2"
numberSwitch 1 = "That's 1"
numberSwitch x = "Not between 1 and 5"



